FROM arm32v7/node:10-stretch

RUN mkdir /usr/src/app

WORKDIR /tmp
RUN wget abyz.me.uk/rpi/pigpio/pigpio.tar \
    && tar xf pigpio.tar
WORKDIR /tmp/PIGPIO
RUN make && make install \
    && rm -rf /tmp/pigpio.tar /tmp/PIGPIO

WORKDIR /usr/src/app
